import 'package:flutter/material.dart';

class AddStudent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Add Student"),
      ),
      body: new Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[
            TextFormField(
              style: TextStyle(
                color: Colors.teal,
                fontSize: 20.0
              ),
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "Student Name"
              ),
            )
          ],
        ),
      ),
    );
  }
}
