import 'package:fee_tracker/add_student.dart';
import 'package:flutter/material.dart';

void main() =>
    runApp(MaterialApp(
      title: "Fee Tracker",
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      theme: ThemeData(primarySwatch: Colors.teal),
    ));

class HomePage extends StatelessWidget {

  void _navigateToAddStudents(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => new AddStudent()));
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
        length: 2,
        child: new Scaffold(
          floatingActionButton: FloatingActionButton(
            elevation: 5.0,
            onPressed: () {
              _navigateToAddStudents(context);
            },
            backgroundColor: Colors.green,
            child: Icon(Icons.add_a_photo),
          ),
          body: NestedScrollView(
            headerSliverBuilder: (BuildContext context,
                bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  title: Text("Fee Tracker"),
                  forceElevated: innerBoxIsScrolled,
                  pinned: true,
                  floating: true,
                  bottom: TabBar(
                    tabs: <Widget>[Tab(text: "9th"), Tab(text: "10th")],
                  ),
                )
              ];
            },
            body: TabBarView(
              children: <Widget>[
                Container(
                  color: Theme
                      .of(context)
                      .accentColor,
                ),
                Container(
                    color: Theme
                        .of(context)
                        .primaryColor
                )
              ],
            ),
          ),
        ));
  }
}
